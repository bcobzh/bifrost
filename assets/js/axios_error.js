export default function axiosError(error) {
    return {
        status: error.response.status,
        statusText: error.response.statusText,
        method: error.request.method,
        path: error.request.path
    }
}
