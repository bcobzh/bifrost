import axios from '~/plugins/axios'
import _ from 'lodash';
import axiosError from '~/assets/js/axios_error';

export const state = () => ({
    sites: [],
    amods: {},
    tplFilters: [],
    pltFilters: []

});
export const getters = {
    sites(state) {
        return state.sites
    },
    amods(state) {
        return state.amods
    },
};
export const mutations = {
    addMessage(state, newMessage) {
        state.messages.push(newMessage);
    },
    addMessages(state, newMessages) {
        state.messages.push(...newMessages);
    },
    SET_SITES(state, sites) {
        state.sites = sites
    },
    ADD_AMODS(state, template) {
        state.amods[template.name] = template.mods;
    },
    SET_ALL_AMODS(state, templates) {
        for (var i = 0; i < templates.length; i++) {
            state.amods[templates[i].name] = templates[i].mods;
        }
    },
    SET_TPL_FILTERS(state, tplFilters) {
        state.tplFilters = tplFilters
    },
    SET_PLT_FILTERS(state, pltFilters) {
        state.pltFilters = pltFilters
    }
};
export const actions = {
    addTplAmod({
        commit
    }, template) {
        commit('ADD_AMODS', template)
    },
    getSites({
        commit
    }) {
        axios.get('/api/sites')
            .then(response => {
                var sites = response.data;
                console.log(sites)
                axios.get('/api/templates/amods')
                    .then(response => {
                        const templates = response.data;
                        _.map(sites, function(site) {
                            const tpl = templates.filter((item) => {
                                return item.name == site.template
                            })[0];
                            if(tpl === undefined) {
                                site["amods"] = [];
                            }else{
                                site.amods = tpl.mods === undefined ? [] :  tpl.mods;
                            }
                        })
                        const pltFilters = _.chain(sites)
                            .map(function(site) {
                                return site.platform
                            })
                            .uniq()
                            .map(function(template) {
                                return {
                                    text: template,
                                    value: template
                                }
                            })
                            .value();
                        const tplFilters = _.chain(sites)
                            .map(function(site) {
                                return site.template
                            })
                            .uniq()
                            .map(function(template) {
                                return {
                                    text: template,
                                    value: template
                                }
                            })
                            .value();
                        commit('SET_SITES', sites)
                        commit('SET_PLT_FILTERS', pltFilters);
                        commit('SET_TPL_FILTERS', tplFilters);
                        commit('SET_ALL_AMODS', templates);

                    })
                    .catch(error => {
                        console.log(error)
                    });
            }).catch(error => {

                console.log(axiosError(error))
            });
    }
};
