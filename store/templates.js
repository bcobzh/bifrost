import axios from '~/plugins/axios'
import _ from 'lodash';

export const  state = () => ( {
    templates: [],

});
export const getters = {
    get(state) { return state.templates },
    getOne(state, name) {
        found = state.templates.fitler(item => item.name === name);
        if (found.length < 1){
            return {}
        }else{
            return found[0]
        }
    }
                         
};
export const mutations = {
    SET_TEMPLATES(state, templates){
        state.templates = templates
    },
    ADD_TEMPLATE (state, template) {
        state.templates.push(template);
    },
};
export const actions = {
    addPlatform({ commit }, template) {
        commit('ADD_AMODS', template)
    },
    getTemplates({ commit }) {
         axios.get('/api/templates')
             .then(response => {
                 commit('SET_TEMPLATES', response.data)
             }).catch(error => {
                 console.log(error)
             });
    }
};

