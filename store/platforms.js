import axios from '~/plugins/axios'
import _ from 'lodash';

export const  state = () => ( {
    platforms: [],

});
export const getters = {
    get(state) { return state.platforms },
    getOne(state, name) {
        found = state.platforms.fitler(item => item.name === name);
        if (found.length < 1){
            return {}
        }else{
            return found[0]
        }
    }
                         
};
export const mutations = {
    SET_PLATFORMS(state, platforms){
        state.platforms = platforms
    },
    ADD_PLATFORM (state, platform) {
        state.platforms.push(platform);
    },
};
export const actions = {
    addPlatform({ commit }, platform) {
        commit('ADD_AMODS', platform)
    },
    getPlatforms({ commit }) {
         axios.get('/api/platforms')
             .then(response => {
                 commit('SET_PLATFORMS', response.data)
             }).catch(error => {
                 console.log(error)
             });
    }
};

