import Vue from 'vue'

Vue.filter('jsonhead', (val, n) => { return JSON.stringify(val).substring(0,n) + '.....'})
